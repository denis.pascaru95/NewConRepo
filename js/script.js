let navMenu = document.getElementById('menu-btn');

navMenu.onclick = () => {
    let navListItem = document.getElementById('navbar-collapse');
    
    if(navListItem.classList.contains('dropdown')) {
        navListItem.classList.remove('dropdown');
    } else {
        navListItem.classList.add('dropdown');
    }
}